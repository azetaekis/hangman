#! /usr/bin/env python3
import random
from os import path


states = [
    """
  +---+
  |   |
      |
      |
      |
      |
=========""",
    """
  +---+
  |   |
  O   |
      |
      |
      |
=========""",
    """
  +---+
  |   |
  O   |
  |   |
      |
      |
=========""",
    """
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========""",
    """
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========""",
    """
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========""",
    """
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========""",
]

red_text = "\033[91m"
green_text = "\033[92m"
normal_text = "\033[00m"

basepath = path.dirname(__file__)


def select_word() -> str:
    """
    It opens the words file, reads the lines, strips some non alpha characters,
    and returns a random word
    :return: A random word from the words.txt file.
    """
    with open(path.join(basepath, "words.txt"), "r") as f:
        words = f.readlines()
        word = (
            random.choice(words).strip("\n").strip("\r").strip("[]").strip("''").lower()
        )
    return word


def validate_input(input: str) -> bool:
    """
    "If the length of the input is 1 and the input is a letter, return True,
    otherwise return False."
    :param input: str
    :type input: str
    :return: A boolean value.
    """
    return True if len(input) == 1 and input.isalpha() else False


def hangman():
    """
    The function `hangman()` runs one instance of the hangman game and returns
    a string `"won"` or `"lost"` depending on the outcome of the game
    :return: a string, either "won" or "lost"
    """
    word_list = list(select_word())
    attempt_list = ["_" for _ in range(len(word_list))]
    guess_list = []
    failed_attempts = 0

    print("Welcome to the Hangman game")
    print(states[failed_attempts])
    print(" ".join(attempt_list))
    print()
    while failed_attempts < 6:
        guess = input("Guess a letter: ").lower()
        if not validate_input(guess):
            print(f"\n{guess} is not allowed. Please enter a letter")
        elif guess in guess_list:
            print(
                f"Already guessed {guess}. This is what you tried: {' '.join(guess_list)}\n"
            )
        else:
            guess_list.append(guess)
            if guess in word_list:
                occurences = [
                    index
                    for index in range(len(word_list))
                    if word_list[index] == guess
                ]
                for index in occurences:
                    attempt_list[index] = guess
                print(f"Great, you guessed {guess}!")
            else:
                failed_attempts += 1
                print(f"Wrong guess, {guess} is not in the word.")
        print(states[failed_attempts])
        print(" ".join(attempt_list))
        print()
        if failed_attempts == 6:
            print(
                f"{red_text}You lost! The word was: {''.join(word_list)}{normal_text}\n"
            )
            return "lost"
        elif attempt_list == word_list:
            print(
                f"{green_text}You guessed the word: {''.join(word_list)}. You won!{normal_text}\n"
            )
            return "won"


def play_loop():
    """
    It runs the hangman game, then asks the user if they want to play again.
    If they do, it runs the game again. If they don't, it quits
    """
    result = hangman()
    prompt = "Do you want to play again?\nPress Y or N"
    print(f"You {result}!\n{prompt}")
    while True:
        play_again = str(input("Y/N: ")).lower()
        if play_again == "y":
            result = hangman()
            print(f"You {result}!\n{prompt}")
        elif play_again == "n":
            print("Thank you for playing. See you next time!")
            quit()
        else:
            print(f"Wrong input.\n{prompt}")


play_loop()
