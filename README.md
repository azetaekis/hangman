# HANGMAN GAME

This is a simple python implementation of the famous Hangman game. As per the requirements:

1. Single player game against computer.
2. Computer will choose a word and the player will try to guess.
3. Until the player wants to quit, they should be able to continue with a new round

## Setup

There is no need to install any dependencies, only python 3 is needed. It has been developed with python3.8 (due to it being the default python version in many distros) and has been tested with python3.6 too. But any python version newer than 3.6 should work too.

To run `flake8` (linter) and `black` (formatter), which are being used in CI checks, a Pipfile is provided, which can be set up by doing:

```bash
pip3 install pipenv
pipenv sync -d

# Lint
flake8 hangman.py

# Format
black hangman.py
```

## Running the game

To run the game simply type `python3 hangman.py` or `./hangman.py` from the current directory. But it can be invoked from different directories too.
